from itertools import combinations
n = int(input())
ls = []
res = 0
for i in range(n):
    a, b = map(int, input().split())
    ls.append([a, b])

comb = combinations(ls, 2)
for i in list(comb):
    x = (i[1][1] - i[0][1]) / (i[1][0] - i[0][0])
    if x >= -1 and x <= 1:
        res += 1

print(res)