#include <iostream>
#include <vector>
#include <algorithm>
#include <stdlib.h>

using namespace std;

void solve() {
	int n;
	cin >> n;
	int a[n], b[n];
	int sum = 0;
	for(int i = 0; i < n; i++){
		cin >> a[i];
	}
	for(int i = 0; i < n; i++) {
		cin >> b[i];
	}
	for(int i = 0; i < n; i++) {
		sum += a[i]*b[i];
	}
	cout << (sum == 0 ? "Yes" : "No") << "\n";
}

int main() {
	solve();
}
