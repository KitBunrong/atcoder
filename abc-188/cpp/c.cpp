#include <bits/stdc++.h>
using namespace std;
#define int long long int
#define dep(x) cout << " #x" <<x<< endl;

void solve() {
	int n1;
	cin >> n1;
	int n  = pow(2, n1);
	int a[n];
	for(int i = 0; i < n; i++) cin >> a[i];
	vector<int> temp;
	for(int i=1; i <= n; i++) temp.push_back(i);
	
	int ans = 0;
	
	int mx1 = *max_element(a, a+n/2);
	int mx2 = *max_element(a+n/2, a+n);
	
	int f = (mx1 < mx2) ? mx1 : mx2;
	
	for(int i=0; i < n; i++) if(a[i] == f) {ans = i+1; break;}
	cout << ans << endl;
}

signed main() {
	solve();
}
