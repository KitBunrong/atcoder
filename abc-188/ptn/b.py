n = int(input())
A = input().split()
B = input().split()
temp = 0
for x in range(n):
    temp += int(A[x])*int(B[x])
print("Yes" if temp == 0 else "No")