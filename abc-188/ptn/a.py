x, y = map(int, input().split())
mn = min(x,y)
mx = max(x, y)
if (mn + 3) > mx:
    print("Yes")
else: 
    print("No")