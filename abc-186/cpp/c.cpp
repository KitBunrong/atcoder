#include <bits/stdc++.h>
using namespace std;

bool d(int x) {
	while(x) {
		int mod = x % 10;
		if(mod == 7) {
			return 1;
		}
		x /= 10;
	}
	return 0;
}

bool o(int x) {
	while(x) {
		int mod = x % 8;
		if(mod == 7) {
			return 1;
		}
		x /= 8;
	}
	return 0;
}

void solved() {
	int n;
	cin >> n;
	int cnt = 0;
	for(int i = 1; i <= n; i++) {
		if(d(i) || o(i)) {
			cnt += 1;
		}
	}
	cout << n - cnt << endl;
}

int main() {
	solved();
	return 0;
}
