#include <bits/stdc++.h>
using namespace std;

void solved() {
	int h, w;
	cin >> h >> w;
	int arr[h][w];
	int mn = INT_MAX;
	int sum = 0;
	
	for(int i = 0; i < h; ++i) {
		for(int j = 0; j < w; ++j) {
			cin >> arr[i][j];
			mn = min(mn, arr[i][j]);
		}
	}
	
	for(int i = 0; i < h; ++i) {
		for(int j = 0; j < w; ++j) {
			sum += arr[i][j] - mn;
		}
	}
	
	cout << sum << endl;
}

int main() {
	solved();
}
